#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/virtio.h>
#include <linux/virtio_balloon.h>
#include <linux/swap.h>
#include <linux/kthread.h>
#include <linux/freezer.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/balloon_compaction.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/proc_fs.h>
#include<linux/sched.h>


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Esha Choukse");
MODULE_DESCRIPTION("A Compresso Linux module.");
MODULE_VERSION("0.01");


#define BALLOON_ARRAY_PFNS_MAX 450000
#define BALLOON_PAGE_ORDER 0 
#define MY_GFP GFP_KERNEL
//8*2048*4096B
int len,temp;
long overall_count;
char *msg;
struct compresso_balloon
{
    /* The array of pfns we tell the Host about. */
    unsigned int num_pfns;
    unsigned long pfns[BALLOON_ARRAY_PFNS_MAX];
};
static struct compresso_balloon *vb;

static void fill_balloon(struct compresso_balloon *vb)
{
    /* We can only do one array worth at a time. */
    for (; vb->num_pfns < overall_count; vb->num_pfns += 1) {
		vb->pfns[vb->num_pfns]=__get_free_pages( GFP_KERNEL, BALLOON_PAGE_ORDER);

		if(!vb->pfns[vb->num_pfns]) {
			printk(KERN_INFO "ERROR in memory allocation -- insufficient memory %u, esha\n", vb->num_pfns);
			overall_count = vb->num_pfns;
			vb->num_pfns-=1;
	
			break;
		}
			
		printk(KERN_INFO "page number %lx %lx, esha\n", vb->pfns[vb->num_pfns], virt_to_phys((void*)vb->pfns[vb->num_pfns]));
    }

}

static void empty_balloon(struct compresso_balloon *vb)
{
    for (; vb->num_pfns > overall_count;
         vb->num_pfns -= 1) {
		//printk(KERN_INFO "Freeing Page number %lu, Esha\n", vb->pfns[vb->num_pfns-1]);
		free_pages(vb->pfns[vb->num_pfns-1], BALLOON_PAGE_ORDER);
    }
}

ssize_t read_proc(struct file *filp,char __user *buf,size_t count,loff_t *offp ) 
{
	char *tempMsg = kmalloc(GFP_KERNEL,10*sizeof(char));
	sprintf(tempMsg, "%ld\n", overall_count);
	if(count>temp)
	{
		count=temp;
	}
	temp=temp-count;
	copy_to_user(buf,tempMsg, strlen(tempMsg));
	if(count==0)
		temp=len;
	kfree(tempMsg);
	return count;
}

ssize_t write_proc(struct file *filp,const char __user *buf,size_t count,loff_t *offp)
{
	long id;
	char *tempMsg = kmalloc(GFP_KERNEL,10*sizeof(char));
	copy_from_user(msg,buf,count);
	len=count;
	temp=len;
	if(msg[0]=='-') {
		//msg=msg+1;
		strncpy(tempMsg, msg+1, count-2);
		tempMsg[count-2]='\0';
		kstrtol(tempMsg, 10, &id);
		printk(KERN_INFO "Deflate value= %ld, Esha\n", id);
		overall_count=overall_count- (id >> (12+BALLOON_PAGE_ORDER));
		if (overall_count < 0)
			overall_count=0;
		empty_balloon(vb);
	}
	else {
		strncpy(tempMsg, msg, count-1);
		tempMsg[count-1]='\0';
		kstrtol(tempMsg, 10, &id);
		overall_count=overall_count+ (id >> (12+BALLOON_PAGE_ORDER));
		printk(KERN_INFO "Inflate value=%ld, %ld pages, overall pages %ld, Esha\n", id, id >> (12+BALLOON_PAGE_ORDER), overall_count);
		if (overall_count > BALLOON_ARRAY_PFNS_MAX) {
			overall_count = BALLOON_ARRAY_PFNS_MAX;
			printk(KERN_INFO "ERROR : Setting ballooning to allowed max value, overall pages %ld, Esha\n", overall_count);
		}
		fill_balloon(vb);
	}
	kfree(tempMsg);
	return count;
}

struct file_operations proc_fops = {
	read: read_proc,
	write: write_proc
};

void my_create_new_proc_entry(void) 
{
	proc_create("compresso",0,NULL,&proc_fops);
	msg=kmalloc(GFP_KERNEL,10*sizeof(char));
}

int proc_init (void) {
	int err;
	my_create_new_proc_entry();
	overall_count=0;

	printk(KERN_INFO "Hello, Esha!\n");
	vb = kmalloc(sizeof(*vb), GFP_KERNEL);
	if (!vb) {
        err = -ENOMEM;
        goto out;
    }
    vb->num_pfns = 0;

	return 0;

	out:
    return err;
}

void proc_cleanup(void) {
	overall_count=0;
	empty_balloon(vb);	
	remove_proc_entry("compresso",NULL);
	kfree(vb);
	kfree(msg);
	printk(KERN_INFO "Goodbye, Esha!\n");
}

module_init(proc_init);
module_exit(proc_cleanup);

