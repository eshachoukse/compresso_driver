obj-m += compresso.o
MY_CFLAGS += -g -DDEBUG
ccflags-y += ${MY_CFLAGS}
CC += ${MY_CFLAGS}
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
#	make -C ~/Documents/Research/OnSim/linux-stable/debian/linux-image-3.12.13+/lib/modules/3.12.13+/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
#	make -C ~/Documents/Research/OnSim/linux-stable/debian/linux-image-3.12.13+/lib/modules/3.12.13+/buil M=$(PWD) clean
test:
	sudo dmesg -C
	sudo insmod compresso.ko
	sudo rmmod compresso.ko
	dmesg
run:
	sudo dmesg -C
	sudo insmod compresso.ko
	dmesg
kill:
	sudo rmmod compresso.ko
